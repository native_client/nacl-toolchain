/* Wrapper to linux-thread-db.c for i386-nacl.

   Copyright (C) 2009 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* This file contains wrappers to the target_ops routines in linux-thread-db.c.
   It exists because we need to force the mode to service-runtime mode
   when making calls to libthread_db.

   This file may not be i386 specific, but for now it doesn't matter.  */

#include "defs.h"
#include "target.h"
#include "i386-nacl-tdep.h"

/* This module's target vector.  */
static struct target_ops nacl_thread_ops;

/* The target vector that we call for things this module can't handle.  */
static struct target_ops linux_thread_db_ops;

static void
nacl_thread_detach (char *args, int from_tty)
{
  struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();

  /* Override the current gdb state, we need libthread_db to see the
     service-runtime view.  */
  set_sr_gdb_state ();

  linux_thread_db_ops.to_detach (args, from_tty);

  do_cleanups (my_cleanups);
}

static void
nacl_thread_resume (ptid_t ptid, int step, enum target_signal signo)
{
  struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();

  /* Override the current gdb state, we need libthread_db to see the
     service-runtime view.  */
  set_sr_gdb_state ();

  linux_thread_db_ops.to_resume (ptid, step, signo);

  do_cleanups (my_cleanups);
}

static ptid_t
nacl_thread_wait (ptid_t ptid, struct target_waitstatus *ourstatus)
{
  ptid_t result_ptid;
  struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();

  /* Override the current gdb state, we need libthread_db to see the
     service-runtime view.  */
  set_sr_gdb_state ();

  result_ptid = linux_thread_db_ops.to_wait (ptid, ourstatus);

  do_cleanups (my_cleanups);

  return result_ptid;
}

static void
nacl_thread_kill (void)
{
  struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();

  /* Override the current gdb state, we need libthread_db to see the
     service-runtime view.  */
  set_sr_gdb_state ();

  linux_thread_db_ops.to_kill ();

  do_cleanups (my_cleanups);
}

static void
nacl_thread_mourn_inferior (void)
{
  struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();

  /* Override the current gdb state, we need libthread_db to see the
     service-runtime view.  */
  set_sr_gdb_state ();

  linux_thread_db_ops.to_mourn_inferior ();

  do_cleanups (my_cleanups);
}

static void
nacl_thread_find_new_threads (void)
{
  struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();

  /* Override the current gdb state, we need libthread_db to see the
     service-runtime view.  */
  set_sr_gdb_state ();

  linux_thread_db_ops.to_find_new_threads ();

  do_cleanups (my_cleanups);
}

static char *
nacl_thread_pid_to_str (ptid_t ptid)
{
  char *str;
  struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();

  /* Override the current gdb state, we need libthread_db to see the
     service-runtime view.  */
  set_sr_gdb_state ();

  str = linux_thread_db_ops.to_pid_to_str (ptid);

  do_cleanups (my_cleanups);

  return str;
}

static CORE_ADDR
nacl_thread_get_thread_local_address (ptid_t ptid,
					 CORE_ADDR lm,
					 CORE_ADDR offset)
{
  CORE_ADDR addr;
  struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();

  /* Override the current gdb state, we need libthread_db to see the
     service-runtime view.  */
  set_sr_gdb_state ();

  addr = linux_thread_db_ops.to_get_thread_local_address (ptid, lm, offset);

  do_cleanups (my_cleanups);

  return addr;
}

static char *
nacl_thread_extra_thread_info (struct thread_info *info)
{
  char *str;
  struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();

  /* Override the current gdb state, we need libthread_db to see the
     service-runtime view.  */
  set_sr_gdb_state ();

  str = linux_thread_db_ops.to_extra_thread_info (info);

  do_cleanups (my_cleanups);

  return str;
}

/* This method is required (see add_target).
   All we have to do is pass the request to the layer beneath us
   (the process layer).  */

static LONGEST
nacl_thread_xfer_partial (struct target_ops *ops, enum target_object object,
			     const char *annex,
			     gdb_byte *readbuf, const gdb_byte *writebuf,
			     ULONGEST offset, LONGEST len)
{
  return ops->beneath->to_xfer_partial (ops->beneath, object, annex,
					readbuf, writebuf, offset, len);
}

static void
fill_nacl_thread_ops (struct target_ops *t)
{
  t->to_shortname = "nacl-multi-thread";
  t->to_longname = "nacl multi-threaded child process.";
  t->to_doc = "NaCl threads support.";
  t->to_detach = nacl_thread_detach;
  t->to_resume = nacl_thread_resume;
  t->to_wait = nacl_thread_wait;
  t->to_kill = nacl_thread_kill;
  t->to_mourn_inferior = nacl_thread_mourn_inferior;
  t->to_find_new_threads = nacl_thread_find_new_threads;
  t->to_pid_to_str = nacl_thread_pid_to_str;
  t->to_stratum = thread_stratum;
  t->to_has_thread_control = tc_schedlock;
  t->to_get_thread_local_address = nacl_thread_get_thread_local_address;
  t->to_extra_thread_info = nacl_thread_extra_thread_info;
  t->to_magic = OPS_MAGIC;

  /* The above is what linux-thread-db.c initializes.
     It relies upon add_target to set to_xfer_partial.
     We don't call add_target (see i386_nacl_add_target),
     so we set to_xfer_partial here.  */
  t->to_xfer_partial = nacl_thread_xfer_partial;
}

void
nacl_init_thread_db_ops (struct target_ops *thread_target)
{
  linux_thread_db_ops = *thread_target;

  fill_nacl_thread_ops (thread_target);
}

/* Provide a prototype to silence -Wmissing-prototypes.  */
extern void _initialize_i386_nacl_thread (void);

void
_initialize_i386_nacl_thread (void)
{
}
