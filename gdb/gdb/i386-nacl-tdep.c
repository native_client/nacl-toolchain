/* Target-dependent code for NativeClient i386.

   Copyright (C) 2008, 2009 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include "defs.h"
#include "gdbcore.h"
#include "frame.h"
#include "value.h"
#include "regcache.h"
#include "inferior.h"
#include "osabi.h"
#include "reggroups.h"
#include "dwarf2-frame.h"
#include "gdb_string.h"
#include "gdbcmd.h"
#include "symfile.h"
#include "completer.h"
#include "objfiles.h"
#include "observer.h"
#include "exec.h"
#include "event-top.h"
#include "exceptions.h"
#include "gdb_proc_service.h"
#include "symtab.h"
#include "linux-nat.h"
#include "solist.h"
#include "readline/tilde.h" /* tilde_expand */
#include "elf/external.h"
#include "elf/common.h"
#include "i386-tdep.h"
#include "i386-nacl-tdep.h"

/* State to be saved/restored across nacl/runtime mode switches.
   NOTE: This does not contain LIVE state.  Instead it contains the
   state at the time we switched out of that mode.  */

struct gdb_state
{
  /* From objfiles.c.  */
  struct objfile *object_files;
  struct objfile *current_objfile;
  struct objfile *symfile_objfile;
  /* Not relevant, but for completeness' sake ...  */
  struct objfile *rt_common_objfile;

  /* From exec.c.  */
  bfd *exec_bfd;

  /* From solib.c.  */
  struct so_list *so_list_head;

  /* Other things of note that aren't saved but need to be managed.

     target.c:target_dcache - flushed on each mode switch (FIXME: TODO)
  */
};

/* Saved copy of service-runtime gdb state.  */
static struct gdb_state sr_gdb_state;

/* Saved copy of nacl-client gdb state.  */
static struct gdb_state nacl_gdb_state;

/* Zero if current state is service-runtime state,
   Non-zero if current state is nacl-client state.  */
static int current_gdb_state_is_nacl = 0;

struct cmd_list_element *nacl_list;
struct cmd_list_element *set_nacl_list;
struct cmd_list_element *show_nacl_list;

/* Prompt used when debugging the native-client side.  */
static char *native_client_prompt;

/* The offset that nacl threads are loaded at.
   This is like their LMA.  */
static ULONGEST xlate_base = 0;

/* This is the value of the segment regs while in service-runtime mode.
   When libthread_db calls ps_lgetregs we need to restore these values.  */
static unsigned int service_runtime_cs;
static unsigned int service_runtime_ds;
static unsigned int service_runtime_es;
static unsigned int service_runtime_fs;
static unsigned int service_runtime_gs;
static unsigned int service_runtime_ss;

/* Non-zero if specified file to debug on command line is a native program.
   Zero -> native-client program.
   This is determined from the binary.
   As such we don't provide a command line option to specify it;
   we don't want to have to continue to support such an option.  */
int native_debugging = 1;

/* Path to sel_ldr.
   The default value is set later, it needs to live in malloc space.
   NOTE: This must be set before the call to nacl_notify_new_executable.
   Thus it is a command-line option.  */
char *sel_ldr_path;

/* A copy of sel_ldr_path for use by set/show service-runtime-path.
   We want to support "show" but not "set" (as we may eventually support
   "set"), but there's no way to intercept "set" before it updates the
   value.  */
static char *copy_sel_ldr_path;

/* List of sel_ldr arguments.
   The default value is set later, it needs to live in malloc space.  */
char *sel_ldr_args;

/* In native mode this is the path to the nacl binary.  */
char *nacl_program_path;

/* Initialize the service-runtime/native-client switching mechanism.  */

static void
init_nacl_gdb_state (void)
{
  memset (&nacl_gdb_state, 0, sizeof (nacl_gdb_state));

  memset (&sr_gdb_state, 0, sizeof (sr_gdb_state));

  current_gdb_state_is_nacl = 0;
}

static void
switch_gdb_state (struct gdb_state *from, struct gdb_state *to)
{
  from->object_files = object_files;
  from->current_objfile = current_objfile;
  from->symfile_objfile = symfile_objfile;
  from->rt_common_objfile = rt_common_objfile;;
  from->exec_bfd = exec_bfd;
  from->so_list_head = master_so_list ();

  object_files = to->object_files;
  current_objfile = to->current_objfile;
  symfile_objfile = to->symfile_objfile;
  rt_common_objfile = to->rt_common_objfile;
  exec_bfd = to->exec_bfd;
  set_so_list (to->so_list_head);
}

void
set_sr_gdb_state (void)
{
  /* No-op if already there.  */
  if (! current_gdb_state_is_nacl)
    return;

  switch_gdb_state (&nacl_gdb_state, &sr_gdb_state);
  current_gdb_state_is_nacl = 0;
}

void
set_nacl_gdb_state (void)
{
  /* No-op if already there.  */
  if (current_gdb_state_is_nacl)
    return;

  switch_gdb_state (&sr_gdb_state, &nacl_gdb_state);
  current_gdb_state_is_nacl = 1;
}

void
set_nacl_gdb_state_for_ptid (ptid_t ptid)
{
  if (ptid_nacl_mode_p (ptid))
    set_nacl_gdb_state ();
  else
    set_sr_gdb_state ();
}

static void
do_cleanup_restore_gdb_state (void *orig_state_ptr)
{
  int *orig_state = orig_state_ptr;

  if (*orig_state)
    set_nacl_gdb_state ();
  else
    set_sr_gdb_state ();

  xfree (orig_state);
}

struct cleanup *
make_cleanup_restore_gdb_state (void)
{
  int *orig_state = (int *) xmalloc (sizeof (int));

  *orig_state = current_gdb_state_is_nacl;
  return make_cleanup (do_cleanup_restore_gdb_state, orig_state);
}

/* Call when the symbol table is cleared.  */

void
nacl_symbol_file_clear (int from_tty)
{
  /* FIXME: Anything to do?  */
}

/* Observer callback for when a new program is run,
   or when a process is attached to.  */

static void
nacl_handle_inferior_created (struct target_ops *ops, int from_tty)
{
  /* Reset the indicator of whether the nacl app has been loaded,
     in case we're re-running or re-attaching.  */
  xlate_base = 0;

  /* Start out in service-runtime mode.  */
  set_sr_gdb_state ();

  /* In case we attached.  */
  nacl_check_app_loaded ();
}

/* Fetch the value of sel_ldr's nacl_global_xlate_base.
   Returns zero if there's a problem in fetching its value.

   The value is stored in a global so that we don't require debug info,
   just symbol info.  */

static ULONGEST
get_xlate_base (void)
{
  struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();
  struct minimal_symbol *m;
  ULONGEST xlate_base_tmp;

  /* Enable the service-runtime symbol table so we can find
     nacl_global_xlate_base.  */
  set_sr_gdb_state ();

  m = lookup_minimal_symbol ("nacl_global_xlate_base", NULL, NULL);
  if (m == NULL)
    {
      do_cleanups (my_cleanups);
      return 0;
    }

  /* If we can find the minsym, we assume the following will work.
     Perhaps it should be wrapped in TRY_CATCH, but it's left out to
     see if it really is necessary.  */
  xlate_base_tmp = parse_and_eval_long ("*(unsigned*) &nacl_global_xlate_base");

  do_cleanups (my_cleanups);
  return xlate_base_tmp;
}

/* Initialize the things we can't do until the nacl program is loaded.

   NOTE: This function can only be called once per run/attach.  */

static void
nacl_init_runtime_state (void)
{
  struct objfile *objfile;
  struct obj_section *osect;
  int flags = 0;

  /* This function can only be called once per run/attach.
     [Technically speaking, it *could* be called more than once, but
     that would be a bit sloppy and indicative of a problem somewhere.  */
  gdb_assert (! nacl_loaded_p ());

  xlate_base = get_xlate_base ();
  if (xlate_base == 0)
    return;

  /* Save the value of %gs.  We need ps_lgetregs to return this value.  */
  {
    struct regcache *regcache;
    ULONGEST sreg;

    regcache = get_thread_regcache (inferior_ptid);
    regcache_raw_read_unsigned (regcache, I386_CS_REGNUM, &sreg);
    service_runtime_cs = sreg;
    regcache_raw_read_unsigned (regcache, I386_DS_REGNUM, &sreg);
    service_runtime_ds = sreg;
    regcache_raw_read_unsigned (regcache, I386_ES_REGNUM, &sreg);
    service_runtime_es = sreg;
    regcache_raw_read_unsigned (regcache, I386_FS_REGNUM, &sreg);
    service_runtime_fs = sreg;
    regcache_raw_read_unsigned (regcache, I386_GS_REGNUM, &sreg);
    service_runtime_gs = sreg;
    regcache_raw_read_unsigned (regcache, I386_SS_REGNUM, &sreg);
    service_runtime_ss = sreg;
  }

  if (native_debugging)
    {
      struct cleanup *my_cleanups = make_cleanup_restore_gdb_state ();

      set_nacl_gdb_state ();
      objfile = symbol_file_add (nacl_program_path, 0, NULL, 0, flags);

      do_cleanups (my_cleanups);
    }
  else
    {
      if (current_gdb_state_is_nacl)
	objfile = symfile_objfile;
      else
	objfile = nacl_gdb_state.symfile_objfile;
    }

  /* Fiddle with the LMAs to fake an overlay.  */
  ALL_OBJFILE_OSECTIONS (objfile, osect)
    {
      bfd *obfd = objfile->obfd;
      asection *bsect = osect->the_bfd_section;

      bfd_section_lma (obfd, bsect) = bsect->vma + xlate_base;
    }

  /* Add the file's sections to our section table.
     ??? It's too bad there isn't a symbol_file_add fn that does this.  */
  {
    struct section_table *start;
    struct section_table *end;
    int count;

    start = NULL;
    if (build_section_table (objfile->obfd, &start, &end) != 0)
      error ("Unable to build section table for NaCl program");

    count = end - start;
    if (count > 0)
      {
	int space = target_resize_to_sections (&current_target, count);
	memcpy (current_target.to_sections + space,
		start,
		count * sizeof (*start));
      }

    xfree (start);
  }

  /* Nacl breakpoints get marked pending when we start to run because
     we don't know the address (akin to shared library breakpoints).
     We know them now, so reset all the addresses.  */
  breakpoint_re_set ();

  /* Getting new symbols may change our opinion about what is frameless.  */
  reinit_frame_cache ();
}

/* Called when we hit _ovly_debug_event.

   NOTE: It's important that this function only be called once per
   inferior run.  */

void
nacl_handle_ovly_debug_event (void)
{
  nacl_init_runtime_state ();
}

/* Called when we attach to a program.
   See if the nacl program has been loaded and if so initialize
   our runtime state.  */

void
nacl_check_app_loaded (void)
{
  /* The nacl app has been loaded if xlate_base has been set
     (to non-zero).  */
  if (get_xlate_base () != 0)
    nacl_init_runtime_state ();
}

/* Return non-zero if we're currently switched to nacl mode.  */

int
nacl_mode_p (void)
{
  return current_gdb_state_is_nacl;
}

/* Return non-zero if thread PTID is in nacl-mode
   (as opposed to service-runtime mode).  */

int
ptid_nacl_mode_p (ptid_t ptid)
{
  struct regcache *regcache;
  ULONGEST ds;
  ULONGEST cs;
  int result;
  volatile struct gdb_exception e;

  if (! target_has_execution)
    return 0;

  /* We reference service_runtime_{cs,ds} below.
     Don't reference them until they're valid.  */
  if (! nacl_loaded_p ())
    return 0;

  /* This may be called to help compute the prompt text.
     If reading regs from the target error's out we'll be in an
     finite loop of trying to get the prompt text and failing.
     If this fails, just assume we're in service-runtime mode for now.  */

  result = 0;

  TRY_CATCH (e, RETURN_MASK_ALL)
    {
      regcache = get_thread_regcache (ptid);
      regcache_raw_read_unsigned (regcache, I386_DS_REGNUM, &ds);
      if (ds != service_runtime_ds)
	{
	  result = 1;
	}
      else
	{
	  regcache_raw_read_unsigned (regcache, I386_CS_REGNUM, &cs);
	  if (cs != service_runtime_cs)
	    result = 1;
	}
    }

  return result;
}

/* Return non-zero if the native-client program has been loaded.  */

int
nacl_loaded_p (void)
{
  return xlate_base != 0;
}

/* This is called by handle_inferior_event when we know the target has
   stopped.  It updates current_gdb_state_is_nacl.  We need to update
   it early, before breakpoint detection, because we need
   software_breakpoint_inserted_here_p to see the correct objfiles;
   it calls section_is_mapped which loops over all objfiles.

   NOTE: Because we need to be called before
   software_breakpoint_inserted_here_p we are called before
   adjust_pc_after_break.

   This also handles stops at _ovly_debug_event.  GDB provides
   bp_overlay_event, but it doesn't provide any way to hook into
   handling it.  Blech.  */

void
nacl_handle_inferior_event (const struct target_waitstatus *ws, ptid_t ptid)
{
  switch (ws->kind)
    {
    case TARGET_WAITKIND_EXITED:
    case TARGET_WAITKIND_SIGNALLED:
      /* Program has terminated.  */
      if (native_debugging)
	set_sr_gdb_state ();
      else
	set_nacl_gdb_state ();
      return;
    case TARGET_WAITKIND_STOPPED:
      break;
    default:
      /* ??? What to do?  */
      return;
    }

  /* Set the current nacl mode.
     This is needed to let symbol lookups while we're stopped use
     the right symbol table.  */

  set_nacl_gdb_state_for_ptid (ptid);
}

char *
get_native_client_prompt (void)
{
  return native_client_prompt;
}

/* FIXME: When porting this to more arches, redo some of this as it's
   not i386-specific.  */

void
i386_nacl_add_target (struct target_ops *t)
{
  add_target (t);

  /* This is here because linux-nat.c:linux_nat_add_target does this.
     See the TODO there though.  */
  thread_db_init (t);
}

char *
get_nacl_prompt (void)
{
  if (! target_has_execution)
    {
      if (native_debugging)
	return PROMPT (0);
      else
	return get_native_client_prompt ();
    }
  else if (ptid_nacl_mode_p (inferior_ptid))
    {
      /* FIXME: This is the wrong place to do this.
	 We should do it at the places where it can change:
	 - execution stops
	 - current thread changes
	 - ???
	 I believe this code is outdated though.  */
      set_nacl_gdb_state ();
      return get_native_client_prompt ();
    }
  else
    {
      set_sr_gdb_state ();
      return PROMPT (0);
    }
}

ULONGEST
get_nacl_thread_base_offset (void)
{
  return xlate_base;
}

/* Update the segment regs used by the service runtime.
   This is the value that libthread_db needs.  */

struct regcache *
i386_nacl_set_sr_seg_regs (struct regcache *p)
{
  struct regcache *r = regcache_dup_no_passthrough (p);

  if (nacl_loaded_p ())
    {
      /* FIXME: There's no current way to do what we want to do, which is to
	 supply a modified register set to fill_gregset (see ps_lgetregs).
	 So we temporarily hack the value of readonly so we can
	 do what we want to do.  */
      regcache_set_readonly (r, 0);
      regcache_raw_supply_unsigned (r, I386_CS_REGNUM, service_runtime_cs);
      regcache_raw_supply_unsigned (r, I386_DS_REGNUM, service_runtime_ds);
      regcache_raw_supply_unsigned (r, I386_ES_REGNUM, service_runtime_es);
      regcache_raw_supply_unsigned (r, I386_FS_REGNUM, service_runtime_fs);
      regcache_raw_supply_unsigned (r, I386_GS_REGNUM, service_runtime_gs);
      regcache_raw_supply_unsigned (r, I386_SS_REGNUM, service_runtime_ss);
      regcache_set_readonly (r, 1);
    }

  return r;
}

static void
nacl_command (char *args, int from_tty)
{
  printf_unfiltered
    ("\"nacl\" must be followed by the name of a nacl command.\n");
  help_list (nacl_list, "nacl ", -1, gdb_stdout);
}

static void
nacl_apply_runtime_command (char *args, int from_tty)
{
  struct cleanup *cleanups = make_cleanup_restore_gdb_state ();

  set_sr_gdb_state ();
  execute_command (args, from_tty);
  do_cleanups (cleanups);
}

static void
nacl_apply_command (char *args, int from_tty)
{
  struct cleanup *cleanups = make_cleanup_restore_gdb_state ();

  set_nacl_gdb_state ();
  execute_command (args, from_tty);
  do_cleanups (cleanups);
}

static void
set_nacl (char *arg, int from_tty)
{
  printf_unfiltered (_("\"set nacl\" must be followed by the name of a \"set nacl\" subcommand.\n"));
  help_list (set_nacl_list, "set nacl ", -1, gdb_stdout);
}

static void
show_nacl (char *args, int from_tty)
{
  cmd_show_list (show_nacl_list, from_tty, "");
}

static void
set_native_client_prompt (char *args, int from_tty, struct cmd_list_element *c)
{
  /* FIXME: Anything else to do?  */
}

static void
show_native_client_prompt (struct ui_file *file, int from_tty,
			   struct cmd_list_element *c, const char *value)
{
  fprintf_filtered (file, _("The native-client prompt is \"%s\".\n"), value);
}

static void
set_sel_ldr_path (char *args, int from_tty, struct cmd_list_element *c)
{
  error ("The path of the service-runtime loader must be set\n"
	 "via the --loader command line option.");
}

static void
show_sel_ldr_path (struct ui_file *file, int from_tty,
		   struct cmd_list_element *c, const char *value)
{
  fprintf_filtered (file,
		    _("The path of the service-runtime loader is \"%s\".\n"),
		    sel_ldr_path);
}

static void
i386_nacl_init_abi (struct gdbarch_info info, struct gdbarch *gdbarch)
{
  i386_linux_init_abi (info, gdbarch);
}

/* Return non-zero if HEADER is a native-client file.  */

static int
nacl_elf_p (Elf64_External_Ehdr *header)
{
  return (header->e_ident[EI_MAG0] == ELFMAG0
          && header->e_ident[EI_MAG1] == ELFMAG1
          && header->e_ident[EI_MAG2] == ELFMAG2
          && header->e_ident[EI_MAG3] == ELFMAG3
          && header->e_ident[EI_OSABI] == ELFOSABI_NACL);
}

/* Return non-zero if HEADER is an ELF file.  */

static int
elf_p (Elf64_External_Ehdr *header)
{
  return (header->e_ident[EI_MAG0] == ELFMAG0
          && header->e_ident[EI_MAG1] == ELFMAG1
          && header->e_ident[EI_MAG2] == ELFMAG2
          && header->e_ident[EI_MAG3] == ELFMAG3);
}

/* Return non-zero if ABFD is a native-client file.

   PERF: This is called by section_is_overlay and thus can get called
   many times.  If this becomes a perf issue, we can use bfd->usrdata
   to cache various nacl-related things.  */

int
nacl_bfd_p (bfd *abfd)
{
  Elf64_External_Ehdr header;

  if (bfd_seek (abfd, 0, SEEK_SET) == 0
      && bfd_bread (&header, sizeof (header), abfd) == sizeof (header)
      && nacl_elf_p (&header))
    return 1;

  return 0;
}

/* Return non-zero if SECTION is a from native-client file.  */

int
nacl_section_p (asection *sec)
{
  return nacl_bfd_p (sec->owner);
}

/* Prepare to load symbols for a new executable.

   There's a sequencing issue here.
   We need to prepare for new symbols to be added but we need to know the
   kind of executable (service-runtime or native-client) before we can add
   the symbols.  This must be called when we have the bfd of the new
   executable but before allocate_objfile is called, we need object_files
   set for sr/nc mode.
   Similarily for setting exec_bfd: We need to know the kind of executable
   before we can set exec_bfd - it needs to be set in the right mode.  */

void
nacl_notify_new_executable (bfd *new_exec_bfd)
{
  Elf64_External_Ehdr header;

  if (new_exec_bfd != NULL
      && bfd_seek (new_exec_bfd, 0, SEEK_SET) == 0
      && bfd_bread (&header, sizeof (header), new_exec_bfd) == sizeof (header))
    {
      if (nacl_elf_p (&header))
	{
	  /* If we're currently debugging a native program,
	     switch to debugging a nacl program.  */
	  if (native_debugging)
	    {
	      /* First clear both sets of symbols.  */
	      set_sr_gdb_state ();
	      /* Pass zero for from_tty because we don't want the query
		 at this point.  */
	      symbol_file_clear (0);
	      set_nacl_gdb_state ();
	      symbol_file_clear (0);

	      /* Everything has been cleared, reset our saved state.  */
	      init_nacl_gdb_state ();

	      /* Before we set the nacl mode to native-client, install sel_ldr
		 on the service-runtime side of things.
		 ??? I think this is why we need to have sel_ldr_path set
		 via a command line option.  But can't we defer setting up
		 the service-runtime side of things?  */
	      {
		char *path = tilde_expand (sel_ldr_path);
		make_cleanup (xfree, path);
		set_sr_gdb_state ();
		/* Note: If sel_ldr_path is invalid, this will error().  */
		exec_file_open (path, 0);
		symbol_file_add (path, 0, NULL, 1, 0);
	      }

	      /* Reset this AFTER calling exec_file_attach,symbol_file_add,
		 so that when we recurse back into here the next if (for
		 non-nacl elfs) will DTRT.
		 ??? Perhaps we could call something lower level than
		 exec_file_attach,symbol_file_add.  Maybe later.  */
	      native_debugging = 0;
	    }

	  set_nacl_gdb_state ();
	  return;
	}
      else if (elf_p (&header))
	{
	  /* If we're currently debugging a nacl program,
	     switch to debugging a native program.  */
	  if (! native_debugging)
	    {
	      /* First clear both sets of symbols.  */
	      set_sr_gdb_state ();
	      /* Pass zero for from_tty because we don't want the query
		 at this point.  */
	      symbol_file_clear (0);
	      set_nacl_gdb_state ();
	      symbol_file_clear (0);

	      /* Everything has been cleared, reset our saved state.  */
	      init_nacl_gdb_state ();

	      native_debugging = 1;
	    }

	  set_sr_gdb_state ();
	  return;
	}

      /* fall through */
    }

  /* This is not an ELF file.
     Don't change anything, just load the file in the current mode.
     This can happen for things like "file foo.srec".  */
}

/* Provide a prototype to silence -Wmissing-prototypes.  */
extern void _initialize_i386_nacl_tdep (void);

void
_initialize_i386_nacl_tdep (void)
{
  struct cmd_list_element *c;

  init_nacl_gdb_state ();

  overlay_debugging = ovly_nacl;

  /* Only set sel_ldr_path if not set via command line option.  */
  if (sel_ldr_path == NULL)
    sel_ldr_path = xstrdup (DEFAULT_SEL_LDR_PATH);
  /* Initialize copy_sel_ldr_path to something so gdb doesn't crash.  */
  copy_sel_ldr_path = xstrdup ("I'm sorry Hal, you *can* do that.");
  sel_ldr_args = xstrdup (DEFAULT_SEL_LDR_ARGS);
  nacl_program_path = xstrdup ("native-client-path unset");

  add_prefix_cmd ("nacl", class_support, nacl_command,
		  _("\
Commands for nacl.\n\
\n\
GDB for Native Client is a little different than normal GDB.\n\
When debugging Native Client programs there are two separate programs:\n\
the \"service runtime\", sel_ldr_bin, and the Native Client program itself.\n\
Each have their own set of symbols and their own \"address space\", and GDB\n\
needs to keep them separate.\n\
GDB does this by maintaining two separate sets of symbols, and having two\n\
\"modes\" depending on the current value of $pc.\n\
If the program is stopped inside the service runtime then the service\n\
runtime's symbols are active.  If the program is stopped inside the native\n\
client then the native client's symbols are active.\n\
\n\
To assist in knowing which is active, GDB uses different prompts to\n\
identify the current mode.\n\
(sr-gdb) <- indicates the current mode is service-runtime\n\
(nc-gdb) <- indicates the current mode is native-client\n\
\n\
Breakpoints in either mode are distinguished by prefixing the address with\n\
\"sr\" or \"nc\".\
"),
		  &nacl_list, "nacl ", 0 /*allow_unknown*/, &cmdlist);

  add_prefix_cmd ("nacl", no_class, set_nacl,
		  _("Generic command for setting nacl flags"),
		  &set_nacl_list, "set nacl ", 0, &setlist);

  add_prefix_cmd ("nacl", no_class, show_nacl,
		  _("Generic command for showing nacl flags"),
		  &show_nacl_list, "show nacl ", 0, &showlist);

  native_client_prompt = xstrdup ("(nc-gdb) ");
  add_setshow_string_cmd ("prompt", class_support,
			  &native_client_prompt, _("\
Set the prompt to use when debugging native-client code"), _("\
Show the prompt to use when debugging native-client code"), NULL,
			  set_native_client_prompt,
			  show_native_client_prompt,
			  &set_nacl_list, &show_nacl_list);

  c = add_cmd ("apply-runtime", class_run, nacl_apply_runtime_command,
	       _("\
Temporarily switch to service-runtime mode and apply a command."),
	       &nacl_list);
  set_cmd_completer (c, command_completer);

  c = add_cmd ("apply", class_run, nacl_apply_command,
	       _("\
Temporarily switch to native-client mode and apply a command."),
	       &nacl_list);
  set_cmd_completer (c, command_completer);

  /* While we currently don't support setting sel_ldr_path except as a command
     line option, we still support displaying the current value.
     We use "set/show" (and have "set" flag an error) instead of "info" in
     case we one day _do_ support setting sel_ldr_path this way.  */
  add_setshow_filename_cmd ("service-runtime-path", no_class, &copy_sel_ldr_path, _("\
Set the path of the runtime loader"), _("\
Show the path of the runtime loader"), NULL,
			    set_sel_ldr_path, show_sel_ldr_path,
			    &set_nacl_list, &show_nacl_list);

  add_setshow_filename_cmd ("service-runtime-args", no_class, &sel_ldr_args, _("\
Set the arguments of the sel_ldr program"), _("\
Show the arguments of the sel_ldr program"), NULL,
			    NULL, NULL,
			    &set_nacl_list, &show_nacl_list);

  add_setshow_filename_cmd ("native-client-path", no_class, &nacl_program_path, _("\
Set the path of the native client program.\n\
For use when debugging sel_ldr_bin itself.\n\
You still have to specify all the arguments to sel_ldr_bin, including the\n\
native client program to run.  This option exists to tell gdb what the\n\
native client program is.\n\
Example:\n\
bash$ nacl-gdb sel_ldr_bin\n\
(gdb) set nacl native-client-path ~/hello.nexe\n\
(gdb) run ~/hello.nexe\n\
[...]\
"), _("\
Show the path of the nacl program"), NULL,
			    NULL, NULL,
			    &set_nacl_list, &show_nacl_list);

  gdbarch_register_osabi (bfd_arch_i386, 0, GDB_OSABI_NACL,
			  i386_nacl_init_abi);

  observer_attach_inferior_created (nacl_handle_inferior_created);
}
