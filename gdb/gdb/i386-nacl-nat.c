/* Native-dependent code for NativeClient i386.

   Copyright (C) 2008, 2009 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include "defs.h"
#include "inferior.h"
#include "regcache.h"
#include "inf-ptrace.h"
#include "linux-nat.h"
#include "i386-tdep.h"
#include "i386-nacl-tdep.h"
#include "i386-linux-nat.h"

/* NOTE: Proper partitioning of *-tdep.c vs *-nat.c code is wip.  */

/* The underlying target ops.
   Need to pass some requests on to.  */
static struct target_ops i386_linux_nat_target_ops;

/* Attempt a transfer all LEN bytes starting at OFFSET between the
   inferior's OBJECT:ANNEX space and GDB's READBUF/WRITEBUF buffer.
   Return the number of bytes actually transferred.  */

static LONGEST
i386_nacl_nat_xfer_partial (struct target_ops *ops,
			    enum target_object object,
			    const char *annex, gdb_byte *readbuf,
			    const gdb_byte *writebuf,
			    ULONGEST offset, LONGEST len)
{
  /* NOTE: OFFSET cannot already contain the base offset.
     This gets called for "x/4xb 0x102c0", for example, and these requests
     end up here with offset == 0x102c0, and this routine can't distinguish
     "x/4xb hello_world" vs "x/4xb 0x102c0".  */

  /* NOTE: This gets called from libthread_db to fetch libpthread data.
     It's i386-nacl-thread.c's job to ensure we're in service-runtime mode
     before we get called. */

  /* If we're in native-client mode, adjust the address by adding the
     load-address of the native client code to OFFSET.  */
  if (nacl_mode_p ())
    offset += get_nacl_thread_base_offset ();

  return i386_linux_nat_target_ops.to_xfer_partial (ops, object, annex,
						    readbuf, writebuf,
						    offset, len);
}

static int
i386_nacl_insert_breakpoint (struct bp_target_info *bp_tgt)
{
  struct gdbarch *gdbarch = current_gdbarch;
  int val;
  const gdb_byte *bp;
  int bplen;
  CORE_ADDR addr;

  /* Determine appropriate breakpoint contents and size for this address.  */
  bp = gdbarch_breakpoint_from_pc
       (gdbarch, &bp_tgt->placed_address, &bp_tgt->placed_size);
  gdb_assert (bp != NULL);

  addr = bp_tgt->placed_address;

  /* Don't bypass memory_xfer_partial, the dcache may be active.
     Breakpoints are always recorded in "service runtime" mode.
     That is, for native-client breakpoints, the breakpoint address is the LMA.
     Adjust the address so i386_nacl_nat_xfer_partial will DTRT.  Blech.  */
  if (nacl_mode_p ())
    addr -= get_nacl_thread_base_offset ();

  /* Save the memory contents.  */
  bp_tgt->shadow_len = bp_tgt->placed_size;
  val = target_read_memory (addr, bp_tgt->shadow_contents,
			    bp_tgt->placed_size);

  /* Write the breakpoint.  */
  if (val == 0)
    val = target_write_memory (addr, bp, bp_tgt->placed_size);

  return val;
}

static int
i386_nacl_remove_breakpoint (struct bp_target_info *bp_tgt)
{
  CORE_ADDR addr = bp_tgt->placed_address;

  /* Don't bypass memory_xfer_partial, the dcache may be active.
     Breakpoints are always recorded in "service runtime" mode.
     That is, for native-client breakpoints, the breakpoint address is the LMA.
     Adjust the address so i386_nacl_nat_xfer_partial will DTRT.  Blech.  */
  if (nacl_mode_p ())
    addr -= get_nacl_thread_base_offset ();

  return target_write_memory (addr, bp_tgt->shadow_contents,
			      bp_tgt->placed_size);
}

/* Start a new inferior child process.  EXEC_FILE is the file to
   run, ALLARGS is a string containing the arguments to the program.
   ENV is the environment vector to pass.  If FROM_TTY is non-zero, be
   chatty about it.

   This function exists to effect the running of nacl programs,
   which require that we run them via sel_ldr.  */

static void
i386_nacl_nat_create_inferior (char *exec_file, char *allargs, char **env,
			       int from_tty)
{
  if (! native_debugging)
    {
      char *new_allargs = concat (sel_ldr_args,
				  " ", exec_file, " ", allargs,
				  NULL);
      struct cleanup *cleanups = make_cleanup (xfree, new_allargs);

      i386_linux_nat_target_ops.to_create_inferior (sel_ldr_path, new_allargs,
						    env, from_tty);

      do_cleanups (cleanups);
    }
  else
    {
      i386_linux_nat_target_ops.to_create_inferior (exec_file, allargs, env,
						    from_tty);
    }
}

/* Return a single-threaded i386-nacl-nat target.

   ??? Another way to go is to have separate targets for linux and nacl,
   and switch b/w them depending on nacl_mode_p ().  I dunno,
   maybe later if it seems like the right thing to do.  */

static struct target_ops *
i386_nacl_nat_target (void)
{
  struct target_ops *t;

  t = i386_linux_target ();

  t = linux_nat_target (t);

  /* Save copy, we need to refer to some of the routines we override.  */
  i386_linux_nat_target_ops = *t;

  /* Install our overrides into t.
     Targets "underneath" us have already cached a pointer to T.
     E.g., inf-prace.c:ptrace_ops_hack.
     GDB's poor-man's version of target inheritance.  */

  t->to_shortname = "nacl-child";
  t->to_longname = "nacl child process";
  t->to_doc = "NaCl process support.";
  t->to_xfer_partial = i386_nacl_nat_xfer_partial;

  /* Insn breakpoints need to be handled specially.
     The memory r/w routines can be called for service-runtime or
     native-client breakpoints, independent of nacl_mode_p ().
     i386_nacl_nat_xfer_partial can't distinguish them.  */
  t->to_insert_breakpoint = i386_nacl_insert_breakpoint;
  t->to_remove_breakpoint = i386_nacl_remove_breakpoint;

  /* When debugging a nacl program "directly" (i.e., native_debugging == 0),
     we have to do a bit of a dance so that we run sel_ldr instead with
     the correct arguments.  */
  t->to_create_inferior = i386_nacl_nat_create_inferior;

  return t;
}

void
_initialize_i386_nacl_nat (void)
{
  struct target_ops *t;

  t = i386_nacl_nat_target ();

  /* Register the target.  */
  i386_nacl_add_target (t);

  /* Copied from i386-linux-nat.c to keep it all in this function.
     ??? Do we need a different version for nacl anyway?  */
  linux_nat_set_new_thread (t, i386_linux_new_thread);
}
