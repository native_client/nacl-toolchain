/* Target-dependent code for NativeClient i386.

   Copyright (C) 2008 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef I386_NACL_TDEP_H
#define I386_NACL_TDEP_H

#include "i386-linux-tdep.h"

#define DEFAULT_SEL_LDR_PATH "~/.mozilla/plugins/sel_ldr_bin"

#define DEFAULT_SEL_LDR_ARGS ""

/* FIXME: This works because top.c includes us.  It's a hack,
   but good enough for now.  */
#define DEFAULT_PROMPT "(sr-gdb) "

/* Non-zero if specified file to debug is a native program.
   Zero -> native-client program.  */
extern int native_debugging;

/* Path to sel_ldr.
   The default value is set later, it needs to live in malloc space.  */
extern char *sel_ldr_path;

/* List of sel_ldr arguments.
   The default value is set later, it needs to live in malloc space.  */
extern char *sel_ldr_args;

struct gdbarch_info;
struct gdbarch;
extern void i386_linux_init_abi (struct gdbarch_info info,
				 struct gdbarch *gdbarch);

/* Return non-zero if ABFD is a native-client file.  */

extern int
nacl_bfd_p (bfd *abfd);

/* Return non-zero if SECTION is a from native-client file.  */

extern int
nacl_section_p (asection *sec);

/* Call when a new main executable is set.  */

extern void
nacl_notify_new_executable (bfd *new_exec_bfd);

/* Call when the symbol table is cleared.  */

extern void
nacl_symbol_file_clear (int from_tty);

/* Called when we hit _ovly_debug_event.  */

extern void
nacl_handle_ovly_debug_event (void);

/* Call after attaching to a process.
   Checks if the nacl app has been loaded yet.  */

extern void
nacl_check_app_loaded (void);

/* Set native-client/service-runtime state for PTID.  */

extern void
set_nacl_gdb_state_for_ptid (ptid_t ptid);

/* Create a cleanup to restore gdb's state
   (which is one of service-runtime or native-client).  */

extern struct cleanup *
make_cleanup_restore_gdb_state (void);

/* Return a nacl thread's base address.  */

extern ULONGEST
get_nacl_thread_base_offset (void);

/* Return a copy of a regcache, with the service-runtime values for the
   segment regs filled in.
   These are the values that libthread_db needs.  */

extern struct regcache *
i386_nacl_set_sr_seg_regs (/*const*/ struct regcache *);

/* Return non-zero if we're currently switched to nacl mode.  */

extern int
nacl_mode_p (void);

/* Return non-zero if thread PTID is in nacl-mode
   (as opposed to service-runtime mode).  */

extern int
ptid_nacl_mode_p (ptid_t);

/* Return non-zero if the native-client program has been loaded.  */

extern int
nacl_loaded_p (void);

/* Called very early by handle_inferior_event.  */

extern void
nacl_handle_inferior_event (const struct target_waitstatus *, ptid_t ptid);

/* Return the prompt to use when debugging service runtime code.
   NOTE: This doesn't return a const char * because get_prompt doesn't.  */

extern char *
get_nacl_prompt (void);

/* Register the nacl target.  */

extern void
i386_nacl_add_target (struct target_ops *);

/* Fill in target_ops for nacl thread support.  */

extern void nacl_init_thread_db_ops (struct target_ops *);

/* Set to native client mode.  */

extern void
set_nacl_gdb_state (void);

/* Set to service runtime mode.  */

extern void
set_sr_gdb_state (void);

#endif /* i386-nacl-tdep.h */
