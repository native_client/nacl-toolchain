/* This program is intended to be started outside of gdb, and then
   attached to by gdb.  Thus, it simply spins in a loop.  The loop
   is exited when & if the variable 'should_exit' is non-zero.  (It
   is initialized to zero in this program, so the loop will never
   exit unless/until gdb sets the variable to non-zero.)
   */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int  should_exit = 0;

#ifdef __native_client__

/* Hack replacement for sleep().  */

unsigned
sleep (unsigned n)
{
  time_t now = time (NULL);
  time_t t;
  do {
    t = time (NULL);
  } while (t < now + n);
  return 0;
}

#endif

int main ()
{
  int  local_i = 0;

  sleep( 10 ); /* System call causes register fetch to fail */
               /* This is a known HPUX "feature"            */
  while (! should_exit)
    {
      local_i++;
    }
  return (0);
}
